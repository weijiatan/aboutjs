define(['jquery'], function($) {

    var Controller = {
        index: function() {

            // 色板居中显示
            var _countMargin = function() {
                var boxW = $('.pick_colors').width();
                var aW = $('.pick_ct a').first().width() + 4;
                if (boxW > 1199) {
                    $('.pick_ct').css('paddingLeft', parseInt((aW + boxW % aW) / 2));
                } else {
                    $('.pick_ct').css('paddingLeft', parseInt((boxW % aW) / 2));
                }
            }

            // 初始化插入数据
            var _initHTML = function() {
                var html = '',
                    colorType = '',
                    colorVal = '',
                    titleVal = '';
                for (var i = 0; i < initDate.items.length; i++) {
                    initDate.items[i].custom_attributes.forEach(function(item, index) {
                        if (item.attribute_code == "url_key") {
                            colorType = item.value.split('-')[0];
                            colorVal = item.value.split('-')[1].substring(0, 6);
                            titleVal = item.value.split('-')[1].substring(7);
                        }
                    })
                    html += '<a alt="' + initDate.items[i].name + '" data-title="' + titleVal + '" name="' + colorVal + '" target="_top" class="widclass colorcls ' + colorType + '" href="http://test.fly-leather.com/catalog/category/view/id/' + initDate.items[i].id + '" style="background:#' + colorVal + '"></a>';
                }
                $('.pick_ct').html(html);
            }

            // 色卡放大
            var _zoomColor = function() {
                var x = 10;
                var y = 20;
                $(".pick_ct a").mouseover(function(e) {
                    var tooltip = "<div id='tooltip'><div class='bg' style='background:#" + this.name + "'></div><div class='txt'>" + $(this).attr('alt') + "</div><div class='title'>" + $(this).attr('data-title') + "</div></div>";
                    $("body").append(tooltip); //把它追加到文档中                        
                    $("#tooltip").css({
                        "top": (e.pageY + y) + "px",
                        "left": (e.pageX + x) + "px"
                    }).show(); //设置x坐标和y坐标，并且显示
                }).mouseout(function() {
                    $("#tooltip").remove(); //移除 
                }).mousemove(function(e) {
                    $("#tooltip").css({
                        "top": (e.pageY + y) + "px",
                        "left": (e.pageX + x) + "px"
                    });
                });
            }

            var _init = function() {

                $('.pick_tools .select').on('click', function() {
                    var obj = $(this).find('ul');
                    if (obj.is(":hidden")) {
                        obj.show();
                    } else {
                        obj.hide();
                    }
                })

                $('.pick_cell1 li').on('click', function() {
                    if (!$(this).hasClass("active")) {
                        $(this).addClass("active").siblings().removeClass("active");
                        window.location.href = $(this).attr('data-url');
                    }
                })
                $('.pick_cell2 li').on('click', function() {
                    if (!$(this).hasClass("active")) {
                        $(this).addClass("active").siblings().removeClass("active");
                        $('.pick_cell2 span').html($(this).html());
                        if ($('.pick_colors').hasClass("small")) {
                            $('.pick_colors').addClass("big").removeClass("small");
                        } else {
                            $('.pick_colors').addClass("small").removeClass("big");
                        }
                        _countMargin();
                    }
                })
                $('.pick_cell3 li').on('click', function() {
                    if (!$(this).hasClass("active")) {
                        $(this).addClass("active").siblings().removeClass("active");
                        $('.pick_cell3 span').html($(this).html());
                        if ($('.pick_colors').hasClass("light")) {
                            $('.pick_colors').addClass("dark").removeClass("light");
                        } else {
                            $('.pick_colors').addClass("light").removeClass("dark");
                        }
                    }
                })

                // 点击页面其他区域，关闭select下拉选项
                $(document).on('click', function(e) {
                    var e = e || window.event; //浏览器兼容性 
                    var elem = e.target || e.srcElement;
                    while (elem) { //循环判断至跟节点，防止点击的是div子元素 
                        if (elem.className && elem.className == 'select') {
                            return;
                        }
                        elem = elem.parentNode;
                    }
                    $('.pick_tools ul').hide();
                });

                // 点击色卡选项
                $('.color_list a').on('click', function() {
                    if ($(this).hasClass("active")) {
                        $(this).removeClass("active")
                    } else {
                        $(this).addClass("active");
                    }

                    var curArr = [];
                    for(var i=0;i<$('.color_list a').length;i++){
                        if($('.color_list a').eq(i).hasClass("active")){
                            curArr.push($('.color_list a').eq(i).attr('data'))
                        }
                    }
                    if(curArr.length){
                        $('.pick_ct a').hide();
                        for(var j=0;j<curArr.length;j++){
                            $('.pick_ct').find('.'+curArr[j]).show();
                        }
                    }else{
                        $('.pick_ct a').show();
                    }
                    
                })

                _initHTML();
                _zoomColor();
                _countMargin();
                $(window).resize(function() {
                    _countMargin();
                });

            };

            _init();

        }
    };
    return Controller;
});