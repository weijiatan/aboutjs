require.config({
    urlArgs:"v=1.0.0",
    paths: {
        "jquery": "./jquery-1.9.1.min",
        "mousewheel": "../libs/jscrollpane/jquery.mousewheel",
        "jscrollpane": "../libs/jscrollpane/jquery.jscrollpane.min",
    },
    shim: {
        'jscrollpane':{
            deps: [
                'css!../libs/jscrollpane/jquery.jscrollpane.css'
            ],
            exports: 'jscrollpane'
        },
    },
    map: {
        '*': {
            'css': './libs/require-css/css.min.js'
        }
    }
});

require(["jquery"], function($) {

    $(function() {
        //加载相应模块
        if (typeof(Config) !== 'undefined' && Config.jsname) {
            require([Config.jsname], function(Controller) {
                Controller[Config.actionname] != undefined && Controller[Config.actionname]();
            });
        }
    });
});