// 倒计时
// countDown(el,{
// 	start:10,
// 	hasMiao:1,
//     callBack: function() {
//         el.innerHTML = '重发验证码';
//     }
// });
export const countDown = (el,opt) => {
    let start = opt.start || 60;
    let callBack = opt.callBack || null;
    let hasMiao = opt.hasMiao || 0;
    let timer = null;
    hasMiao = hasMiao === 0 ? '秒':'';

    timer = setInterval(function () {
        if (start === 1) {
            clearInterval(timer);
            callBack && callBack();
        } else {
            start--;
            el.innerHTML = start + hasMiao;
        }
    }, 1000);
};


// 格式化时间
// formatDate(new Date(),'yyyy-MM-dd hh:mm:ss')
// formatDate(new Date(1535447561000),'yyyy-MM-dd hh:mm:ss')
export const formatDate = (date,format) => {
    var args = {
       "M+": date.getMonth() + 1,
       "d+": date.getDate(),
       "h+": date.getHours(),
       "m+": date.getMinutes(),
       "s+": date.getSeconds(),
       "q+": Math.floor((date.getMonth() + 3) / 3),  //quarter
       "S": date.getMilliseconds()
    };
    if (/(y+)/.test(format))
        format = format.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var i in args) {
        var n = args[i];
        if (new RegExp("(" + i + ")").test(format))
           format = format.replace(RegExp.$1, RegExp.$1.length == 1 ? n : ("00" + n).substr(("" + n).length));
    }
    return format;
};

// 设置cookie
export const setCookie = (name,value,timeout) => {
    var Days = timeout?timeout:10000; 
    var exp = new Date(); 
    exp.setTime(exp.getTime() + Days*24*60*60*1000); 
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString(); 
}

// 获取cookie
export const getCookie = (name) => {
    var cookieValue = "";
    var search = name + "=";
    if (document.cookie.length > 0)
    {
        var offset = document.cookie.indexOf (search);
        if (offset !== -1)
        {
            offset += search.length;
            var end = document.cookie.indexOf (";", offset);
            if(end === -1){
                end = document.cookie.length;
            }
            cookieValue = unescape (document.cookie.substring (offset, end))
        }
    }
    return cookieValue;
}