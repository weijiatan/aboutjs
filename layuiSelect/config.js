require.config({
    urlArgs:"v=1.0.1",
    paths: {
        'layui': 'layui/layui.all',
        'formSelects':'formSelects-v4.min'
    },
    shim: {
        'layui': {
        	exports: 'layui'
        },
        'formSelects':{
            deps: [
                'css!formSelects-v4.css'
            ],
            exports: 'formSelects'
        }
    },
    map: {
        '*': {
            'css': 'require-css/css.min'
        }
    }

});

require(["layui","formSelects"], function(layui,formSelects) {

    // console.log(formSelects.value('select1'));


    layui.use('form', function(){
        var form = layui.form;
        form.on('submit(select1)', function(data){
            var that = this;
            $.ajax({
                url: 'https://www.solux.cn',
                type: "post",
                data: $(that).parents(".layui-form").serialize(),
                dataType:"json",
                success: function (data){

                }
            })
            return false;
        });
        
    });

// formSelects.value('select1');               // [{"name":"上海","val":"2"},{"name":"深圳","val":"4"}]
// formSelects.value('select1', 'all');        // [{"name":"上海","val":"2"},{"name":"深圳","val":"4"}]
// formSelects.value('select1', 'val');        // ["2","4"]
// formSelects.value('select1', 'valStr');     // 2,4
// formSelects.value('select1', 'name');       // ["上海","深圳"]
// formSelects.value('select1', 'nameStr');    // 上海,深圳

});