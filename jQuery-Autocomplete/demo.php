<?php

$a = $_REQUEST['query'];
index($a);

function index($a)
{
    $data = array(
    	'query' => $a,
    	'suggestions' =>  array(
    		array(
	    		'value' => 'United Arab Emirates'
	    	),
	    	array(
	    		'value' => $a.'a'
	    	),
            array(
                'value' => $a.'b'
            ),
            array(
                'value' => $a.'c'
            ),
            array(
                'value' => $a.'d'
            ),
            array(
                'value' => $a.'e'
            ),
            array(
                'value' => $a.'f'
            ),
            array(
                'value' => $a.'g'
            )
    	)
    );
    echo json_encode($data);
}
