require("./index.css");

require('jquery');
require('./select2.min');
require('./zh-CN');

function formatState(state){
    if(!state.id){return state.text;}
    var stateline = $('<span class="fastbannerform__span f32 NOFLAG ' + state.title + '"></span> <span class="fastbannerform__span">' + state.text + '</span>');
    return stateline;
}
$(".shop_country").select2({
    language: "zh-CN",
    templateResult: formatState,
    templateSelection: formatState
});
// 默认选中值的方法
$('.shop_country').val($('.shop_country').attr('data-select')).trigger('change');