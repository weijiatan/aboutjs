let [isOn,HAS_DATA] = [true,true];
let params = {
    'page':1,
    'type':'list'
}

// 插入html
const _valHtml = (item) =>{
    var $container = `<div><span>${item.value}</span></div>`;
    return $container;
}

// 滚动请求下一页数据
const _loadMoreMsg = (url) =>{
    if (!HAS_DATA) {
        return false;
    }
    $(".loadding-tips").show();
    params.page++;
    $.ajax({
        url: url,
        type: 'GET',
        dataType: 'json',
        data: params,
    }).done(function(res) {
        isOn = true;
        if(res.code == 0){
            // 执行插入操作
            var html = '';
            for(var i = 0; i<res.data.length;i++){
                html += _valHtml(res.data[i])
            }
            $('.wrapper').append(html);
        }else{
            // 无数据了
            HAS_DATA = false;
            params.page--;
            $(".loadding-tips").html("Duang～到底了");
        }
    }).fail(function() {
        console.log("请求失败，请刷新重试！");
    })
}

export const loadMsg = (url) => {
    var range = 150; //距下边界长度/单位px
    var totalheight = 0;
    $(window).scroll(function() {
        var srollPos = $(window).scrollTop(); //滚动条距顶部距离(页面超出窗口的高度)
        totalheight = parseFloat($(window).height()) + parseFloat(srollPos);
        if (($(document).height() - range) <= totalheight && HAS_DATA == true) {
            if (isOn) {
                isOn = false;
                _loadMoreMsg(url);
            }
        }
    });
};